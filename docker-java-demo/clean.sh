#!/bin/bash

declare -r IMAGE_NAME="medalibettaieb/java-docker-compus"
declare -r IMAGE_TAG="latest"
declare -r APP_PORT="8123"
declare -r APP_PORT_EXT="8123"
declare -r CONTAINER_NAME="compus-java"

echo "stoping container   '$CONTAINER_NAME'"
docker stop $CONTAINER_NAME

echo "deleting container   '$CONTAINER_NAME'"
docker rm $CONTAINER_NAME

echo "deleting image '$IMAGE_NAME:$IMAGE_TAG'"

 docker rmi -f  $IMAGE_NAME:$IMAGE_TAG