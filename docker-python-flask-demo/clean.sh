#!/bin/bash

declare -r IMAGE_NAME="medalibettaieb/python-docker-compus"
declare -r IMAGE_TAG="latest"
declare -r CONTAINER_NAME="compus-python"

echo "stoping container   '$CONTAINER_NAME'"
docker stop $CONTAINER_NAME

echo "deleting container   '$CONTAINER_NAME'"
docker rm $CONTAINER_NAME

echo "deleting image '$IMAGE_NAME:$IMAGE_TAG'"

 docker rmi -f  $IMAGE_NAME:$IMAGE_TAG