#!/bin/bash

declare -r IMAGE_NAME="medalibettaieb/python-docker-compus"
declare -r IMAGE_TAG="latest"
declare -r APP_PORT="5000"
declare -r APP_PORT_EXT="5000"
declare -r CONTAINER_NAME="compus-python"

echo "Starting container for image '$IMAGE_NAME:$IMAGE_TAG', exposing port $APP_PORT/tcp"
docker run --name $CONTAINER_NAME -d -p $APP_PORT_EXT:$APP_PORT $IMAGE_NAME:$IMAGE_TAG

