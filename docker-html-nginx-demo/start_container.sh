#!/bin/bash

declare -r IMAGE_NAME="medalibettaieb/nginx-docker-demo-compus"
declare -r IMAGE_TAG="latest"
declare -r APP_PORT_EXT="8080"
declare -r APP_PORT="80"
declare -r CONTAINER_NAME="compus-nginx"

echo "Starting container for image '$IMAGE_NAME:$IMAGE_TAG', exposing port $APP_PORT/tcp"
docker run --name $CONTAINER_NAME -d -p $APP_PORT_EXT:$APP_PORT $IMAGE_NAME:$IMAGE_TAG

