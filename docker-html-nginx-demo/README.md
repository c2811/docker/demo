# Tutorial: Create a Docker image for a web application


A template project to create a Docker image for a web application.


# Requirements

Docker must be installed. That's it.


# Usage and Demo

**Step 1:** Create the Docker image according to [Dockerfile](Dockerfile).
docker build -t medalibettaieb/some-content-nginx:1.0 .
**Step 2:** run the container and expose the external port: 8080
docker run --name compus-nginx -d -p 8080:80 medalibettaieb/some-content-nginx:1.0
**Step 3:** Then you can hit http://localhost:8080 or http://host-ip:8080 in your browser.

